
package br.com.senac.exemplo9;


public class ViagemAutomovel {
    
    private static final double KMPORLITRO = 12;
    private double tempo;
    private double velocidade;
    private double distanciaPercorrida;
    private double litrosUtilizados;
    
    public ViagemAutomovel(double HoraTempo, double HoraVelocidade){
        this.tempo = HoraTempo;
        this.velocidade = HoraVelocidade;    
    }
    
    public double DistanciaTotal (){
        this.distanciaPercorrida = this.tempo * this.velocidade;
        return distanciaPercorrida;
    }
    
    public double LitrosGastosTotal(){
        this.litrosUtilizados = DistanciaTotal()/KMPORLITRO;
        return litrosUtilizados;   
    }
}
