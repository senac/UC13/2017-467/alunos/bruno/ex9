/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.exemplo9.test;

import br.com.senac.exemplo9.ViagemAutomovel;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class ViagemAutomovelTest {
    
    @Test
    public void CalcularDistancia(){
        ViagemAutomovel viagem = new ViagemAutomovel(4, 60);
        assertEquals(240,viagem.DistanciaTotal(), 0.02);
       
    }
    
    @Test
    public void CalcularConsumo(){
        ViagemAutomovel viagem = new ViagemAutomovel(4, 60);
        double resultado = viagem.LitrosGastosTotal();
        assertEquals(20,resultado,0.01);
        
    }
    
}
